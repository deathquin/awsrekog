const express = require('express');
const router = express.Router();
const util = require('util');

const AWS = require('aws-sdk');
const congig = AWS.config.loadFromPath("./aws_reckog_config.json");

const awsConfig = new AWS.Config(congig);

//const sns = new AWS.SNS(awsConfig);
const rekognition = new AWS.Rekognition(awsConfig);
const s3 = new AWS.S3(awsConfig);

const NodeCache = require( "node-cache" );
const myCache = new NodeCache();

let info = null;

const aws_access_key_id = congig.credentials.accessKeyId;
const aws_secret_access_key = congig.credentials.secretAccessKey;
const region = 'ap-northeast-1';
const credentials = new AWS.Credentials(aws_access_key_id, aws_secret_access_key);
const awsSNSConfig = new AWS.Config({ credentials, region });
const sns = new AWS.SNS(awsSNSConfig);

/* GET home page. */
router.get('/', function(req, res, next) {

    myCache.get( "myKey", function( err, value ){
        if( !err ){
            if(value === undefined){
                res.send("<script type='text/javascript'>alert('사용자 정보가 없습니다.');window.location.href = '/create/info';</script>");
            }else{
                res.render('index', { title: 'Express' });
            }
        }
    });

});

router.get("/capture", function(req, res){
    res.render("webcam");
});

router.get("/delete",async function(req, res){

    const params = {};
    rekognition.listCollections(params, async function(err, data) {
        if(err) {
            res.json(err);
        } else {
            const collections = data.CollectionIds;

            for(let index in collections) {
                const params = {
                    CollectionId: collections[index]
                };
                await rekognition.deleteCollection(params, function(err, data) {});
            }

            res.json(data.CollectionIds);
        }
    });

});

router.post("/create/image", async function(req, res){

    const body = req.body;
    const im = JSON.parse(body.imageName);

    if(info === null) {
        res.json({result: false, msg: "Collection을 추가해주세요"});
    } else {
        for(let index in im ) {

            console.log(im[index]);
            const params = {
                CollectionId: info.collectionName,
                Image: {
                    S3Object: {
                        Bucket: body.bucketName,
                        Name: im[index],
                    }
                }
            };

            await rekognition.indexFaces(params, function(err, data) {
                if(err) {
                    console.log(err);
                    res.json(err);
                    return;
                }
            });

        }

        res.json({result: true});
    }
});

/*router.get("/create", function(req, res){

    const params = {
        CollectionId: "myphotos"
    };

    rekognition.createCollection(params, function(err, data) {
      if(err) {
        res.json(err);
      } else {
        res.json("create");
      }
    });

});

router.get("/upload", function(req, res){

    // collection Id에 이미지 입력
    const params = {
        CollectionId: 'myphotos', /!* required *!/
        Image: {
            /!* required *!/
            S3Object: {
                Bucket: 'rekogimage',
                Name: 'dongyeong3.jpg',
            }
        }
    };

    rekognition.indexFaces(params, function(err, data) {
        if(err) {
            res.json(err);
        } else {
            res.json(data);
        }
    });

});

 */


router.get("/test2", function(req, res) {

    // collection id에 있는 값에서 현재 검색하는 사진이 있는지 검색
    const params = {
        CollectionId: "myphotos",
        FaceMatchThreshold: 80,
        Image: {
            S3Object: {
                Bucket: "rekogimage",
                Name: "heaji.jpg"
            }
        },
        MaxFaces: 5
    };

    rekognition.searchFacesByImage(params, async function (err, data) {
        if(err) {
            console.log(err);
            res.json(err);
        } else {
            const match = data.FaceMatches.length;
            if(match <= 0 ) {
               await sendSMS(info.phoneNumber,  "인증되지 않은 사용자 입니다.");
            } else {
               await sendSMS(info.phoneNumber,  "인증된 사용자 입니다.");
            }
            res.send("<script type='text/javascript'>alert('인증 결과를 전송 했습니다.');window.location.href = '/test';</script>");
        }
    });

});

router.post("/test", async function(req, res){

    const bucketName = req.body.bucketName;
    const imageName = req.body.imageName;

    const params = {
        CollectionId: info.collectionName,
        FaceMatchThreshold: 80,
        Image: {
            S3Object: {
                Bucket: bucketName,
                Name: imageName
            }
        },
        MaxFaces: 5
    };

    rekognition.searchFacesByImage(params, async function (err, data) {
        if(err) {
            console.log(err);
            res.json({reslut: false, msg: err});
        } else {
            console.log("111");
            const match = data.FaceMatches.length;
            if(match <= 0 ) {
                await sendSMS(info.phoneNumber,  "인증되지 않은 사용자 입니다.");
            } else {
                await sendSMS(info.phoneNumber,  "인증된 사용자 입니다.");
            }
            // res.send("<script type='text/javascript'>alert('인증 결과를 전송 했습니다.');window.location.href = '/test';</script>");
            res.json({reslut: true, msg: '인증 결과를 전송 했습니다.'});
        }
    });

});

router.get("/test", function(req, res) {
    res.render("createSingle");
});

router.get("/create/info", function(req, res){
    res.render("create");
});

router.get("/create/info/multi", function(req, res){
    res.render("createMulti");
});

router.post("/create/info", function(req, res){
    const body = req.body;
    const collectionName = body.collectionName;
    const phoneNumber = body.phoneNumber;

    const params = {
        CollectionId: collectionName
    };

    rekognition.createCollection(params, function(err, data) {
        if(err) {
            res.json({result: false, msg: err});
        } else {
            const obj = { collectionName: collectionName, phoneNumber: phoneNumber };
            myCache.set( "myKey", obj, function( err, success ){
                if( !err && success ){
                    info = obj;
                    // res.send("<script type='text/javascript'>alert('사용자 정보를 등록 하였습니다.');window.location.href = '/create/info/multi';</script>");
                    // res.json("Collection ID = " + collectionName + ", PhoneNumber = " + phoneNumber);
                    res.json({result: true, msg: "사용자 정보를 등록하였습니다."});
                } else {
                    res.json({result: false, msg:"정보를 생성하는데 실패 했습니다."});
                }
            });
        }

    });

});

router.post('/check/exist/bucket', async function(req, res) {
    const bucketName = req.body.bucketName;

    const params = {
        Bucket: bucketName,
    };

    try {
        await s3.headBucket(params).promise();
        res.json({result: true});
    } catch (error) {
        res.json({result: false, msg: error});
    }
});

router.post('/check/exist/image', async function(req, res) {
    const bucketName = req.body.bucketName;
    const imageName = req.body.imageName;

    const params = {
        Bucket: bucketName,
        Key: imageName
    };

    try {
        await s3.headObject(params, function(err, data) {
            if(err) {
                res.json({result: false})
            } else {
                res.json({result: true});
            }
        });
    } catch (err) {
        res.json({result: false, msg: err});
    }
});

router.post('/check/exist/images', async function(req, res) {
    const bucketName = req.body.bucketName;
    const imageNames = JSON.parse(req.body.imageNames);

    try {
        new Promise(async function(resolve, reject) {
            let match = true, i =0;
            for(;i<imageNames.length;i++) {
                const params = {
                    Bucket: bucketName,
                    Key: imageNames[i]
                };

                try {
                    await s3.headObject(params).promise();
                    match = true;
                } catch (err) {
                    match = false;
                    return;
                }
            }
            resolve(match);
        }).then(function(result) {
            return result;
        }).catch(function(err) {
            console.error(err);
        });
    } catch (err) {
        res.json({result: false, msg: err});
    }
});

async function sendSMS(number, message) {

    console.log(number);
    console.log(message);

    const params = {
        MessageStructure: 'string',
        Message: message,
        Subject: "hello",
        PhoneNumber: '+82'+number,
        MessageAttributes: {
            "AWS.SNS.SMS.SenderID" : {
                DataType: "String",
                StringValue : "0asd101004s"
            },
            "AWS.SNS.SMS.SMSType" : {
                DataType: "String",
                StringValue : "Transactional"
            }
        },
    };

    try {
        await sns.publish(params).promise();
        return {result: true};

    } catch(err) {
        console.log(err);
        return {result: false};
    }
}

module.exports = router;
